<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CarteRepository")
 */
class Carte
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $circonference;

    /**
     * @ORM\Column(type="integer")
     */
    private $nb_rivieres;

    /**
     * @ORM\Column(type="integer")
     */
    private $nb_chaine_montagnes;

    /**
     * @ORM\Column(type="integer")
     */
    private $nb_forets;

    /**
     * @ORM\Column(type="integer")
     */
    private $temperature_max;

    /**
     * @ORM\Column(type="integer")
     */
    private $temperature_min;

    /**
     * @ORM\Column(type="datetime")
     */
    private $cree_le;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiee_le;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;
	
	public function __construct()
	{
		$this->cree_le = new \DateTime();
	}
	

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCirconference(): ?int
    {
        return $this->circonference;
    }

    public function setCirconference(int $circonference): self
    {
        $this->circonference = $circonference;

        return $this;
    }

    public function getNbRivieres(): ?int
    {
        return $this->nb_rivieres;
    }

    public function setNbRivieres(int $nb_rivieres): self
    {
        $this->nb_rivieres = $nb_rivieres;

        return $this;
    }

    public function getNbChaineMontagnes(): ?int
    {
        return $this->nb_chaine_montagnes;
    }

    public function setNbChaineMontagnes(int $nb_chaine_montagnes): self
    {
        $this->nb_chaine_montagnes = $nb_chaine_montagnes;

        return $this;
    }

    public function getNbForets(): ?int
    {
        return $this->nb_forets;
    }

    public function setNbForets(int $nb_forets): self
    {
        $this->nb_forets = $nb_forets;

        return $this;
    }

    public function getTemperatureMax(): ?float
    {
        return $this->temperature_max;
    }

    public function setTemperatureMax(float $temperature_max): self
    {
        $this->temperature_max = $temperature_max;

        return $this;
    }

    public function getTemperatureMin(): ?float
    {
        return $this->temperature_min;
    }

    public function setTemperatureMin(float $temperature_min): self
    {
        $this->temperature_min = $temperature_min;

        return $this;
    }

    public function getCreeLe(): ?\DateTimeInterface
    {
        return $this->cree_le;
    }

    public function setCreeLe(\DateTimeInterface $cree_le): self
    {
        $this->cree_le = $cree_le;

        return $this;
    }

    public function getModifieeLe(): ?\DateTimeInterface
    {
        return $this->modifiee_le;
    }

    public function setModifieeLe(\DateTimeInterface $modifiee_le = null): self
    {
        $this->modifiee_le = $modifiee_le;
		if ($modifiee_le == null){
			$this->modifiee_le = new \DateTime();
		}

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
