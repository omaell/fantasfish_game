<?php

namespace App\Controller;

use App\Repository\CarteRepository;
use App\Form\CarteType;
use App\Entity\Carte;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CarteController extends AbstractController
{
	
	/**
	 *  @var CarteRepository
	 */
	private $carte_repo;
	
	/**
	 *  @var ObjectManager
	 */
	private $em;
	
	
	public function __construct(CarteRepository $carte_repo, ObjectManager $em)
	{
		$this->carte_repo = $carte_repo;
		$this->em = $em;
	}
	
    /**
     * @Route("/carte/toutes", name="carte.index")
     */
    public function index()
    {
		$cartes = $this->carte_repo->findAll();
        return $this->render('carte/index.html.twig', [
            'controller_name' => 'CarteController',
			'cartes' => $cartes,
        ]);
    }
	
	/**
     * @Route("/carte/creation", name="carte.crud.create")
     */
	public function createCarte(Request $request)
	{
		$carte = new Carte();
		$carte->setModifieeLe();
		$form = $this->createForm(CarteType::class, $carte);
		$form->handleRequest($request);
		
		if ($form->isSubmitted() && $form->isValid()) {
			$this->em->persist($carte);
			$this->em->flush();
			$this->addFlash('success','Carte créée avec succès !');
			return $this->redirectToRoute("carte.index");
		}	
		
		return $this->render('carte/crud.html.twig', [
            'controller_name' => 'CarteController',
			'form' => $form->createView(),
        ]);
	}
	
	/**
	 *  
	 *  @param [in] $request Request
	 *  @param [in] $carte Carte
	 *  @return RedirectResponse
	 *  
	 *  @Route("/carte/suppression/{id}", name="carte.crud.delete")
	 */
	public function deleteCarte(Request $request, Carte $carte)
	{	
		if ($this->isCsrfTokenValid('delete' . $carte->getId(), $request->get('_token'))){
			$this->em->remove($carte);
			$this->em->flush();
			$this->addFlash('success','Carte supprimée avec succès !');

		}
		return $this->redirectToRoute("carte.index");

		
	}
}
